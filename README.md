# Fucking Goddamn New

[![rubygems][gem-image]][gem] [![build status][ci-image]][ci] [![code quality][cq-image]][cq] [![code quality][cc-image]][cq]

Everyone gets pissed at their code sometimes.

Why shouldn't it work?

## Installation

`$ gem install fucking_goddamn_new`

or add `gem 'fucking_goddamn_new'` to your gemfile

## Usage

```
$ irb
>> require 'fucking_goddamn_new'
#> true
>> Object.fucking.goddamn.new
#> #<Object:0x007fab0602f6d0>
>> Object.fucking(:work).goddammit
#> Object
>> Object.fucking.new
#> #<Object:0x007fab06056ac8>
```

<!-- links -->
[ci]: http://travis-ci.org/BM5k/fucking-goddamn-new "build status"
[cq]: https://codeclimate.com/github/BM5k/fucking-goddamn-new "code quality"
[gem]: http://rubygems.org/gems/fucking_goddamn_new

<!-- images -->
[cc-image]: https://codeclimate.com/github/BM5k/fucking-goddamn-new/coverage.png
[ci-image]: https://travis-ci.org/BM5k/fucking-goddamn-new.svg?branch=master
[cq-image]: https://codeclimate.com/github/BM5k/fucking-goddamn-new.png
[gem-image]: https://badge.fury.io/rb/fucking_goddamn_new.svg
